#!/bin/bash

# First install oh-my-zsh
if [ `which zsh` == "zsh not found" ]; then
    sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

    # Install autosuggestions
    git clone git://github.com/zsh-users/zsh-autosuggestions $ZSH_CUSTOM/plugins/zsh-autosuggestions
fi

# Copy over my zshrc
cp zshrc ~/.zshrc

# Set sublime snippet
rm ~/Library/Application\ Support/Sublime\ Text\ 3/Packages/User/*.sublime-snippet
cp ~/Projects/dotfiles/sublimeSnippets/* ~/Library/Application\ Support/Sublime\ Text\ 3/Packages/User

setup_git () {
    git config --global push.default simple
    git config --global user.name "Luc Hendriks"
    git config --global user.email "luchendriks@gmail.com"
}

setup_git

# Set ssh config
cp sshconfig ~/.ssh/config