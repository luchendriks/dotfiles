" vim-sublime - A minimal Sublime Text -like vim experience bundle
"               http://github.com/grigio/vim-sublime
" Best view with a 256 color terminal and Powerline fonts

set nocompatible
filetype off
set rtp+=/Users/luc/.vim/bundle/Vundle.vim/
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'

Bundle 'tpope/vim-surround'
Bundle 'gcmt/breeze.vim'
Bundle 'kien/ctrlp.vim'
Bundle 'SirVer/ultisnips'
Bundle 'tomtom/tcomment_vim'
Bundle 'bling/vim-airline'
Bundle 'airblade/vim-gitgutter'
Bundle 'scrooloose/nerdtree'
Bundle "wookiehangover/jshint.vim"
Bundle "auto-pairs"
Plugin 'Valloric/YouCompleteMe'
Plugin 'jelera/vim-javascript-syntax'
Plugin 'pangloss/vim-javascript'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'kristijanhusak/vim-hybrid-material'

call vundle#end()            " required
filetype plugin indent on    " required

let g:enable_bold_font = 1

autocmd vimenter * NERDTree

" Color Themes
Bundle 'flazz/vim-colorschemes'
syntax on
filetype plugin indent on

" Tab indentation
set tabstop=4       " The width of a TAB is set to 4.
set shiftwidth=4    " Indents will have a width of 4
set softtabstop=4   " Sets the number of columns for a TAB
set expandtab       " Expand TABs to spaces
set gdefault
set number
set nobackup
set noswapfile
set history=1000
set relativenumber
set cursorline
set showcmd
set guioptions-=T  "remove toolbar
let mapleader = "\<Space>"
set ignorecase
set smartcase
set incsearch
set showmatch
set hlsearch
nnoremap <leader>, :noh<cr>

" Theme + font
set background=dark
colorscheme hybrid_material

" Line below + tabs
let g:airline#extensions#tabline#enabled = 1
map <Left> :bprevious<cr>
map <Right> :bnext<cr>
noremap <Up> <C-w>h
noremap <Down> <C-w>l

" Fuzzyfinder
nmap <c-p> :CtrlP <CR>
let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git\|*.swp\|dist'
" let g:ctrlp_working_path_mode = 'c'

highlight clear SignColumn

" This allows buffers to be hidden if you've modified a buffer.
" This is almost a must if you wish to use buffers in this way.
set hidden

" Close the current buffer and move to the previous one
" This replicates the idea of closing a tab
nmap <leader>q :bp <BAR> bd #<CR>

" Fix some typos and create some shortcuts
nmap :W :w
nmap :Q :q
nnoremap ; :
nnoremap J 20j
nnoremap K 20k

" cut/copy/paste
"set paste
nmap <C-V> "+gP
imap <C-V> <ESC><C-V>i
vmap <C-C> "+y
vmap <C-X> "+x

nnoremap <leader>w <C-w>v<C-w>l "split screen

" Go to end of paste
vnoremap <silent> y y`]
vnoremap <silent> p p`]
nnoremap <silent> p p`]

" vp doesn't replace paste buffer
function! RestoreRegister()
  let @" = s:restore_reg
  return ''
endfunction
function! s:Repl()
  let s:restore_reg = @"
  return "p@=RestoreRegister()\<cr>"
endfunction
vmap <silent> <expr> p <sid>Repl()

" Search always result in middle screen
" http://vim.wikia.com/wiki/Make_search_results_appear_in_the_middle_of_the_screen
nnoremap n nzz
nnoremap N Nzz
nnoremap * *zz
nnoremap # #zz
nnoremap g* g*zz
nnoremap g# g#zz
set scrolloff=5
" Fast shell switching (with ctrl-d)
noremap <C-d> :sh<cr>
set shell=/bin/zsh

let g:netrw_liststyle=3

" JS in HTML highlighting
au BufRead *.html set filetype=html
