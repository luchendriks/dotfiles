# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=/Users/luc/.dotfiles

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="robbyrussell"

HYPHEN_INSENSITIVE="true"
ENABLE_CORRECTION="true"
COMPLETION_WAITING_DOTS="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git zsh-autosuggestions)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='mvim'
fi


# Alias for syncing zshrc
alias zsync="~/Projects/dotfiles/sync.sh;source ~/.zshrc"

# Aliases
alias pubkey="cat ~/.ssh/id_rsa.pub | pbcopy | echo "Copied public key""

# Git aliases
alias gitprune="git remote prune origin && git branch -vv | grep ': gone]' | awk '{print $1}' | xargs git branch -D"
alias gs="git status"
alias gd="git diff"
gitupdate = function() {
    git add .
    git commit -m $1
    git push
}

alias randomToken="echo \"var crypto=require('crypto');console.log(crypto.randomBytes(20).toString('hex'))\" | node"

# IP addresses
alias ip="dig +short myip.opendns.com @resolver1.opendns.com"
alias localip="ipconfig getifaddr en0"
alias ips="ifconfig -a | perl -nle'/(\d+\.\d+\.\d+\.\d+)/ && print $1'"

# Dark Machine
alias darkmachine="ssh luc@darkmachine"
alias darkboard="ssh -L 8080:darkmachine:6006 luc@darkmachine -N"
alias darkmount="sshfs  -o volname=Darkmachine luc@darkmachine:/home/luc ~/Darkmachine"
alias darkunmount="umount luc@darkmachine:/home/luc"